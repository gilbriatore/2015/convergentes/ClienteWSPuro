package br.edu.up;

import br.edu.up.ws.ContadorProxy;

public class ClienteWS {
	
	public static void main(String[] args) throws Exception {
		
		ContadorProxy proxy = new ContadorProxy();
		int v = proxy.next1();
		
		System.out.println("Valor: " + v); 
		
	}
}