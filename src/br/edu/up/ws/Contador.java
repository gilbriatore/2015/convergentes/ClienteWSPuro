/**
 * Contador.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.edu.up.ws;

public interface Contador extends java.rmi.Remote {
    public java.lang.Integer[] nextN(int arg0) throws java.rmi.RemoteException;
    public int next1() throws java.rmi.RemoteException;
}
