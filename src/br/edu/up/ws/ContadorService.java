/**
 * ContadorService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.edu.up.ws;

public interface ContadorService extends javax.xml.rpc.Service {
    public java.lang.String getContadorPortAddress();

    public br.edu.up.ws.Contador getContadorPort() throws javax.xml.rpc.ServiceException;

    public br.edu.up.ws.Contador getContadorPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
