/**
 * ContadorServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.edu.up.ws;

public class ContadorServiceLocator extends org.apache.axis.client.Service implements br.edu.up.ws.ContadorService {

    public ContadorServiceLocator() {
    }


    public ContadorServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ContadorServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ContadorPort
    private java.lang.String ContadorPort_address = "http://localhost:8888/ContadorService";

    public java.lang.String getContadorPortAddress() {
        return ContadorPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ContadorPortWSDDServiceName = "ContadorPort";

    public java.lang.String getContadorPortWSDDServiceName() {
        return ContadorPortWSDDServiceName;
    }

    public void setContadorPortWSDDServiceName(java.lang.String name) {
        ContadorPortWSDDServiceName = name;
    }

    public br.edu.up.ws.Contador getContadorPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ContadorPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getContadorPort(endpoint);
    }

    public br.edu.up.ws.Contador getContadorPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.edu.up.ws.ContadorPortBindingStub _stub = new br.edu.up.ws.ContadorPortBindingStub(portAddress, this);
            _stub.setPortName(getContadorPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setContadorPortEndpointAddress(java.lang.String address) {
        ContadorPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.edu.up.ws.Contador.class.isAssignableFrom(serviceEndpointInterface)) {
                br.edu.up.ws.ContadorPortBindingStub _stub = new br.edu.up.ws.ContadorPortBindingStub(new java.net.URL(ContadorPort_address), this);
                _stub.setPortName(getContadorPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ContadorPort".equals(inputPortName)) {
            return getContadorPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws.up.edu.br/", "ContadorService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws.up.edu.br/", "ContadorPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ContadorPort".equals(portName)) {
            setContadorPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
