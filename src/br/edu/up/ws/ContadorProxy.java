package br.edu.up.ws;

public class ContadorProxy implements br.edu.up.ws.Contador {
  private String _endpoint = null;
  private br.edu.up.ws.Contador contador = null;
  
  public ContadorProxy() {
    _initContadorProxy();
  }
  
  public ContadorProxy(String endpoint) {
    _endpoint = endpoint;
    _initContadorProxy();
  }
  
  private void _initContadorProxy() {
    try {
      contador = (new br.edu.up.ws.ContadorServiceLocator()).getContadorPort();
      if (contador != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)contador)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)contador)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (contador != null)
      ((javax.xml.rpc.Stub)contador)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.edu.up.ws.Contador getContador() {
    if (contador == null)
      _initContadorProxy();
    return contador;
  }
  
  public java.lang.Integer[] nextN(int arg0) throws java.rmi.RemoteException{
    if (contador == null)
      _initContadorProxy();
    return contador.nextN(arg0);
  }
  
  public int next1() throws java.rmi.RemoteException{
    if (contador == null)
      _initContadorProxy();
    return contador.next1();
  }
  
  
}